package com.travel.travelwatamu;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by root on 3/4/18.
 */

public class ServerResults extends Activity implements View.OnClickListener {


    private PendingIntent pendingIntent;
    private AlarmManager manager;
    public TextView pollResults;

    Button start, stop;



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.poll_server);
        setUpvars();
        start.setOnClickListener(this);
        stop.setOnClickListener(this);
    }

    private void setUpvars() {

        start = (Button) findViewById(R.id.bstart);
        stop = (Button) findViewById(R.id.bstop);
        pollResults = (TextView) findViewById(R.id.tvPollSeverResults);
    }

    @Override
    protected void onResume() {
        super.onResume();

        pollResults.setText("");
    }

    /**
 * this method will be called when the alarm is started, at the click of the button
 * and will repeat every 10 seconds
     * @param view

     */

    public void startAlarm(View view) {
        manager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        int interval = 10000;

        Intent alarmIntent = new Intent(this, PollServer.class);
        pendingIntent = PendingIntent.getBroadcast(this, 0, alarmIntent, 0);
        manager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);

        manager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), interval, pendingIntent);
        Toast.makeText(this, "Alarm Set", Toast.LENGTH_SHORT).show();
    }

    public void cancelAlarm(View view) {
//        if (manager != null) {
//            manager.cancel(pendingIntent);
//            Toast.makeText(this, "Alarm Canceled", Toast.LENGTH_SHORT).show();
//        }
        Intent alarmIntent = new Intent(this, PollServer.class);
        pendingIntent = PendingIntent.getBroadcast(this, 0, alarmIntent, 0);
        manager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        manager.cancel(pendingIntent);
        Toast.makeText(this, "Alarm Canceled", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.bstart:

                startAlarm(view);

                break;
            case R.id.bstop:

                cancelAlarm(view);

                break;
        }

    }

    public void updateTextView(String result){
        pollResults.setText(result);
    }
}
