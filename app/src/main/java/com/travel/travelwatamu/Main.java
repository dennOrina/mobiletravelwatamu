package com.travel.travelwatamu;

import android.accounts.Account;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceActivity;
import android.preference.Preference;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.BoolRes;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import java.util.concurrent.ExecutionException;
import java.util.jar.Manifest;

public class Main extends AppCompatActivity implements View.OnClickListener {

    //	public static final String PREFS_NAME = "KalPrefsFile";


    public String identifier = "";
    public String targetUrl = "";
    Boolean permission_status;

    TextView diplayJsonResponse, testConnectionResult;
    Button testConnection;


    public void onResume() {
        Log.d("DiscoucherSMS", "RESUME");
        super.onResume();

        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);

        this.identifier = settings.getString("pref_identifier", "");
        this.targetUrl =  settings.getString("pref_target_url", "");

        Log.d("TravelWatamu", "onResume ident:" + this.identifier +"\ntarget:" + this.targetUrl);

        String infoText = "";

        infoText = "All SMS messages";

        infoText += " are now sent to URL  " + this.targetUrl +"  ";

        testConnectionResult.setText("");
        diplayJsonResponse.setText(infoText);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        PreferenceManager.setDefaultValues(this, R.xml.prefs, false);

        diplayJsonResponse = (TextView) findViewById(R.id.tvDisplayResponse);
        testConnection = (Button) findViewById(R.id.btestConnection);
        testConnectionResult = (TextView) findViewById(R.id.tvTestResult);

        testConnection.setOnClickListener(this);

//        permission_status =  isSmsPermissionGranted();

//        if (permission_status){
//            Log.d("DiscoucherSMS", "SMS PERMISSION GRANTED");
//            return;
//        }else{
//            Log.d("DiscoucherSMS", "REQUESTING SMS PERMISSION FROM USER");
            showRequestPermissionsInfoAlertDialog();
//            requestReadAndSendSmsPermission();
//        }


        Log.d("TravelWatamuSMS", "STARTED");
    }


    // first time the Menu key is pressed
    public boolean onCreateOptionsMenu(Menu menu) {
//        startActivity(new Intent(this, Prefs.class));
//        getMenuInflater().inflate(R.menu.menu_main, menu);

        MenuInflater blowUp = getMenuInflater();
        blowUp.inflate(R.menu.cool_menu, menu);
        return true;

    }

    public boolean onOptionsItemSelected(MenuItem item) {
        // we will use a switching case on the menu ids to identify which is selected and do something
        switch (item.getItemId()) {
            case R.id.aboutUs:
                // an about us activity will be started when this is selected [that means there's an aboutus class and xml and manifest declaration  ]
                Intent i = new Intent("com.travel.travelwatamu.ABOUT");
                startActivity(i);
                break;
            case R.id.preferences:
                // a prefs activity will be started when this is selected
                Intent p = new Intent("com.travel.travelwatamu.PREFS"); // [that means there's a prefs class and xml and manifest declaration ]
                startActivity(p);
                break;
            case R.id.poll_server:

                Intent poll = new Intent("com.travel.travelwatamu.SERVERRESULTS");
                startActivity(poll);
                break;
            case R.id.exit: // this will end the application
                finish(); // this is enough to end the app because at this time only the menu activity will be running
                break;
        }
        return false;
    }



    @Override
    protected void onStop(){
        // dont do much with this, atm..
        super.onStop();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btestConnection:
                TelephonyManager tMgr = (TelephonyManager)this.getSystemService(Context.TELEPHONY_SERVICE);
//                String mPhoneNumber = tMgr.getLine1Number();

                String phone_number = "0711430817";
                String edwin_number = "0780244108";
                String message = "Travel Watamu sms Has just been started";

                if (isNetworkAvailable(this)){
                    Toast.makeText(getApplicationContext(), "You are connected to the internet",
                            Toast.LENGTH_SHORT).show();

                    try {
                        final String result = new SMSReceiver.OpenUrl().execute(phone_number, message, targetUrl).get();
                        testConnectionResult.setText(result);

                        SMSReceiver.sendSms( result, phone_number);
                        SMSReceiver.sendSms( result, edwin_number);
                        Toast.makeText(getApplicationContext(), "You should receive a message shortly",
                                Toast.LENGTH_SHORT).show();
                    } catch (InterruptedException | ExecutionException e) {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(), "Error Sending Test message",
                                Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(getApplicationContext(), "No internet connection!",
                            Toast.LENGTH_LONG).show();
                }




                break;
        }

    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(requestCode == 1){
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED){
                Toast.makeText(this, "Sms Permission Granted", Toast.LENGTH_SHORT);
            }else{
                Toast.makeText(this, "Sms Permission NOT Granted", Toast.LENGTH_SHORT);
            }
        }
    }


/*Check Permissions
*
* does the app have permission to read and send sms?
*
* */
    public boolean isSmsPermissionGranted() {
        return ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_SMS) == PackageManager.PERMISSION_GRANTED;
    }

    /**
     * Request runtime SMS permission
     */
    private void requestReadAndSendSmsPermission() {
        String permission = android.Manifest.permission.READ_SMS;
        int grant = ContextCompat.checkSelfPermission(this, permission);
//        if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
//            showRequestPermissionsInfoAlertDialog();
            // You may display a non-blocking explanation here, read more in the documentation:
            // https://developer.android.com/training/permissions/requesting.html
//        }

        String[] permission_list = new String[1];
        permission_list[0] = permission;
//        request the permission
        ActivityCompat.requestPermissions( this, permission_list, 1);
//        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_SMS}, SMS_PERMISSION_CODE);
    }

    //    Getting run time permissions for sms receiving -----old
    /*private void requestSmsPermission() {
        String permission = android.Manifest.permission.READ_SMS;
        int grant = ContextCompat.checkSelfPermission(this, permission);

        if (grant != PackageManager.PERMISSION_GRANTED){
            String[] permission_list = new String[1];
            permission_list[0] = permission;
            ActivityCompat.requestPermissions( this, permission_list, 1);
        }
    }*/

    public void showRequestPermissionsInfoAlertDialog() {
        permission_status =  isSmsPermissionGranted();
        if (permission_status){
            Log.d("DiscoucherSMS", "SMS PERMISSION GRANTED");
            return;
        }else{
            Log.d("DiscoucherSMS", "REQUESTING SMS PERMISSION FROM USER");
            showRequestPermissionsInfoAlertDialog(true);
        }

    }

    public void showRequestPermissionsInfoAlertDialog(final boolean permission_status) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.permission_alert_dialog_title); // Your own title
        builder.setMessage(R.string.permission_dialog_message); // Your own message

        builder.setPositiveButton(R.string.action_ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                // Display system runtime permission request?
                if (permission_status) {
                    requestReadAndSendSmsPermission();
                }
            }
        });

        builder.setCancelable(false);
        builder.show();
    }
}
