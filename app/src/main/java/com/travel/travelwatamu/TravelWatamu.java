package com.travel.travelwatamu;

import android.app.Application;
import android.content.Context;

/**
 * Created by root on 2/18/18.
 */

public class TravelWatamu extends Application {

    private static Context context;

    public void onCreate() {
        super.onCreate();
        TravelWatamu.context = getApplicationContext();
    }

    public static Context getAppContext() {
        return TravelWatamu.context;
    }
}
