package com.travel.travelwatamu;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.concurrent.ExecutionException;

/**
 * Created by root on 3/4/18.
 */

public class PollServer extends BroadcastReceiver {
    

    public String targetUrl = "";


    @Override
    public void onReceive(Context context, Intent intent) {

        boolean check = Main.isNetworkAvailable(context);

        if (check){
            SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
            this.targetUrl =  settings.getString("pref_target_url", "");


            String phone_number = "0711430817";
            String edwin_number = "0780244108";
            String message = "Are you there?";

            try {
                final String result = new SMSReceiver.OpenUrl().execute(phone_number, message, targetUrl).get();


                SMSReceiver.sendSms( result, phone_number);
//              +  SMSReceiver.sendSms( result, edwin_number);
                Toast.makeText(context, result,
                        Toast.LENGTH_LONG).show();

//                ServerResults

            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
                Toast.makeText(context, "Error Sending Test message",
                        Toast.LENGTH_SHORT).show();
            }
        }else{

            Toast.makeText(context, "No internet connection!",
                    Toast.LENGTH_LONG).show();
        }

    }


}
