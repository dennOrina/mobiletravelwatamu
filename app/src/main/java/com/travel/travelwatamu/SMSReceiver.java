package com.travel.travelwatamu;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

/**
 * Created by root on 2/16/18.
 */

public class SMSReceiver extends BroadcastReceiver {

    @Override
    // source: http://www.devx.com/wireless/Article/39495/1954
    public void onReceive(Context context, Intent intent) {
        if (!intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")) {
            return;

        }
//        Toast.makeText(TravelWatamu.getAppContext(), "Broadcast reciever here",
//                Toast.LENGTH_SHORT).show();

        // get settings
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);

        boolean status = settings.getBoolean("check_on_off",true);
//
//      Listen to incoming messages only if activated
        if (status == true){
            String identifier = settings.getString("pref_identifier", "");
            String targetUrl =  settings.getString("pref_target_url", "");

            SmsMessage messages[] = getMessagesFromIntent(intent);

            for (int i = 0; i < messages.length; i++) {
                SmsMessage mesg = messages[i];
                String message = mesg.getDisplayMessageBody();
                String phone_number = mesg.getDisplayOriginatingAddress();

                if (message != null && message.length() > 0
                        && (message.toLowerCase().startsWith(identifier) || identifier.trim() == "")) {

                    Log.d("TravelWatamuSMS", "MSG RCVD:\"" + message + "\" from: " + phone_number);

                    // send the message to the URL

                    String resp = "";
                    try {
//                  Using asych task to send the message to url. we can't do that from the main thread

                        /*
                         * Should implement
                         * Check if we are connected to the internet and request the to do so if not
                         * */
                        resp = new OpenUrl().execute(phone_number,message,targetUrl).get();
//                    String s = url;
//                    Toast.makeText(Discoucher.getAppContext(),resp , Toast.LENGTH_SHORT).show();
                        Log.d("AAAAAAAAAAAAAA", "RESP:\"" + resp);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    }


                    Log.d("AAAAAAAAAAAAAA", "RESP:\"" + resp);

//                 SMS back the response
                    sendSms(resp, phone_number); //refactored code here.
//                if (resp.trim().length() > 0) {
////                    ArrayList<ArrayList<String>> items = parseXML(resp);
//                    ArrayList<ArrayList<String>> items = parseJSON(resp);
//
//                    SmsManager smgr = SmsManager.getDefault();
//
//
//                    for (int j = 0; j < items.size(); j++) {
//                        String sendTo = items.get(j).get(0);
//                        if (sendTo.toLowerCase() == "phone_number") sendTo = phone_number;
//                        String sendMsg = items.get(j).get(1);
//                        ArrayList<String> parts = smgr.divideMessage(sendMsg);
//                        final int c =parts.size();
//
//
//                        for (int z=0; z < c; z++) {
//                            final String divided_message = parts.get(z);
//
//                            try {
//
//
//                                smgr.sendTextMessage(sendTo, null, divided_message, null, null);  //#send short msg
////                              smgr.sendMultipartTextMessage(sendTo, null, parts, null, null); // send long message that has been devided into parts
//                                Log.d("divided messages: ", String.valueOf(c));
//                            } catch (Exception ex) {
//                                Log.d("TravelWatamuSMS", "SMS FAILED");
//                                ex.printStackTrace();
//                                Toast.makeText(context, "SMS sending failed...",
//                                        Toast.LENGTH_SHORT).show();
//                            }
//                        }
//                    }
//                }
//
//                 delete SMS from inbox, to prevent it from filling up
//                DeleteSMSFromInbox(context, mesg);

                }
            }
        }


    }

    public  String processResponse (String response){
        if (response.trim().length() > 0) {
//                    ArrayList<ArrayList<String>> items = parseXML(resp);
            ArrayList<ArrayList<String>> items = parseJSON(response);

        }
        return response;
    }

    public static void sendSms(String sms, String phone_number) {
        // SMS back the response
        if (sms.trim().length() > 0) {
//                    ArrayList<ArrayList<String>> items = parseXML(resp);
            ArrayList<ArrayList<String>> items = parseJSON(sms);

            SmsManager smgr = SmsManager.getDefault();


            for (int j = 0; j < items.size(); j++) {
                String sendTo = items.get(j).get(0);
                if (sendTo.toLowerCase() == "phone_number") sendTo = phone_number;
                String sendMsg = items.get(j).get(1);
                ArrayList<String> parts = smgr.divideMessage(sendMsg);

                try {
                    Log.d("DisCoucherAAAASMS", "SEND MSG:\"" + sendMsg + "\" TO: " + sendTo);
                    smgr.sendMultipartTextMessage(sendTo, null, parts, null, null);
//                    smgr.sendTextMessage(sendTo, null, sendMsg, null, null);
                } catch (Exception ex) {
                    Log.d("DisCoucherSMS", "SMS FAILED");
                }

            }
        }
    }

    private void DeleteSMSFromInbox(Context context, SmsMessage mesg) {
        Log.d("DisCoucherSMS", "try to delete SMS");

        try {
            Uri uriSms = Uri.parse("content://sms/inbox");

            StringBuilder sb = new StringBuilder();
            sb.append("address='" + mesg.getOriginatingAddress() + "' AND ");
            sb.append("body='" + mesg.getMessageBody() + "'");
            Cursor c = context.getContentResolver().query(uriSms, null, sb.toString(), null, null);
            c.moveToFirst();
            int thread_id = c.getInt(1);
            context.getContentResolver().delete(Uri.parse("content://sms/conversations/" + thread_id), null, null);
            c.close();
        } catch (Exception ex) {
            // deletions don't work most of the time since the timing of the
            // receipt and saving to the inbox
            // makes it difficult to match up perfectly. the SMS might not be in
            // the inbox yet when this receiver triggers!
            Log.d("SmsReceiver", "Error deleting sms from inbox: " + ex.getMessage());
        }
    }

    // from http://github.com/dimagi/rapidandroid
    // source: http://www.devx.com/wireless/Article/39495/1954
    private SmsMessage[] getMessagesFromIntent(Intent intent) {
        SmsMessage retMsgs[] = null;
        Bundle bdl = intent.getExtras();
        try {
            Object pdus[] = (Object[]) bdl.get("pdus");
            retMsgs = new SmsMessage[pdus.length];
            for (int n = 0; n < pdus.length; n++) {
                byte[] byteData = (byte[]) pdus[n];
                retMsgs[n] = SmsMessage.createFromPdu(byteData);
            }

        } catch (Exception e) {
            Log.e("DisCoucherSMS", "GetMessages ERROR\n" + e);
        }
        return retMsgs;
    }

//    public String openURL(final String phone_number, final String message, final String targetUrl) {
//
////        class
//
//        class OpenUrl extends AsyncTask<Void, Void, String> {
//
//            protected void onPostExecute(String s) {
//                super.onPostExecute(s);
//                Toast.makeText(Discoucher.getAppContext(), s, Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            protected String doInBackground(Void... voids) {
//
//
//                List<NameValuePair> qparams = new ArrayList<NameValuePair>();
//                qparams.add(new BasicNameValuePair("phone_number", phone_number));
//                qparams.add(new BasicNameValuePair("message", message));
//                String url = targetUrl + "?" + URLEncodedUtils.format(qparams, "UTF-8");
//
//                try {
//                    HttpClient client = new DefaultHttpClient();
//                    HttpPost get = new HttpPost(url);
//
//                    HttpResponse responseGet = client.execute(get);
//                    HttpEntity resEntityGet = responseGet.getEntity();
//                    if (resEntityGet != null) {
//                        String resp = EntityUtils.toString(resEntityGet);
//                        Log.e("DisCoucherSMS", "HTTP RESP" + resp);
//                        return resp;
//                    }
//
//
//                } catch (Exception e) {
//                    Log.e("DisCoucherSMS", "HTTP REQ FAILED:" + url);
//                    e.printStackTrace();
////            String s = url;
////            Toast.makeText(this, , Toast.LENGTH_SHORT).show();
//                }
////                return null;
//                return "";
//            }
//        }
//
//        return "";
//
//    }

    public static class OpenUrl extends AsyncTask<String, Void, String> {

        protected void onPostExecute(String s) {
            super.onPostExecute(s);
//            Toast.makeText(Discoucher.getAppContext(), s, Toast.LENGTH_SHORT).show();
        }

        @Override
        protected String doInBackground(String... strings) {

            String phone_number = strings[0];
            String message = strings[1];
            String targetUrl = strings[2];

            List<NameValuePair> qparams = new ArrayList<NameValuePair>();
            qparams.add(new BasicNameValuePair("phone_number", phone_number));
            qparams.add(new BasicNameValuePair("message", message));
            String url = targetUrl + "?" + URLEncodedUtils.format(qparams, "UTF-8");

            try {
                HttpClient client = new DefaultHttpClient();
                HttpPost get = new HttpPost(url);

                HttpResponse responseGet = client.execute(get);
                HttpEntity resEntityGet = responseGet.getEntity();
                if (resEntityGet != null) {
                    String resp = EntityUtils.toString(resEntityGet);
                    Log.e("DisCoucherSMS", "HTTP RESP" + resp);
                    return resp;
                }


            } catch (Exception e) {
                Log.e("DisCoucherSMS", "HTTP REQ FAILED:" + url);
                e.printStackTrace();
//            String s = url;
//            Toast.makeText(this, , Toast.LENGTH_SHORT).show();
            }
//                return null;
            return "";
        }
    }



    public static ArrayList<ArrayList<String>> parseXML(String xml) {
        ArrayList<ArrayList<String>> output = new ArrayList<ArrayList<String>>();

        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

            Document doc = dBuilder.parse(new InputSource(new StringReader(xml)));

            NodeList rnodes = doc.getElementsByTagName("reply");

            NodeList nodes = rnodes.item(0).getChildNodes();

            for (int i=0; i < nodes.getLength(); i++) {
                try {
                    List<String> item = new ArrayList<String>();

                    Node node = nodes.item(i);
                    if (node.getNodeType() != Node.ELEMENT_NODE) continue;

                    Element e = (Element) node;
                    String nodeName = e.getNodeName();

                    if (nodeName.equalsIgnoreCase("sms")) {
                        if (!e.getAttribute("phone").equals("")) {
                            item.add(e.getAttribute("phone"));
                            item.add(e.getFirstChild().getNodeValue());
                            output.add((ArrayList<String>) item);
                        }
                    } else if (nodeName.equalsIgnoreCase("sms-to-phone_number")) {
                        item.add("phone_number");
                        item.add(e.getFirstChild().getNodeValue());
                        output.add((ArrayList<String>) item);
                    } else {
                        continue;
                    }
                } catch (Exception e){
                    Log.e("DisCoucherSMS", "FAILED PARSING XML NODE# " + i  );
                }
            }
            Log.e("DisCoucherSMS", "PARSING XML RETURNS " + output );
            return (output);

        } catch (Exception e) {
            Log.e("DisCoucherSMS", "PARSING XML FAILED: " + xml );
            e.printStackTrace();
            return (output);
        }
    }

    public static ArrayList<ArrayList<String>>  parseJSON(String json) {

        ArrayList<ArrayList<String>> jsonoutput = new ArrayList<ArrayList<String>>();

        if (json != null){

            try {
                JSONObject jsonObject = new JSONObject(json);

                JSONArray data = jsonObject.getJSONArray("sms");

                for ( int i = 0; i < data.length(); i++){


                    try {
                        List<String> jsonitem = new ArrayList<String>();
                        JSONObject c = data.getJSONObject(i);

                        String phone = c.getString("phone_number");
                        String text = c.getString("message");
                        jsonitem.add(phone);
                        jsonitem.add(text);

                        jsonoutput.add((ArrayList<String>) jsonitem);
                    } catch (JSONException e) {
                        Log.e("DisCoucherSMS", "FAILED PARSING JSON " + i );
                        e.printStackTrace();
                    }
                }

                Log.e("DisCoucherSMS", "PARSING JSON RETURNS " + jsonoutput );
                return (jsonoutput);
            } catch (JSONException e) {
                Log.e("DisCoucherSMS", "FAILED PARSING JSON " + json  );
                e.printStackTrace();
                return jsonoutput;
            }

        }
        return jsonoutput;
    }
}